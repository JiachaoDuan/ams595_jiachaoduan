#AMS595 project 3
#Jiachao Duan 110177654
import numpy as np
import matplotlib.pyplot as plt
#first we need to initialize the parameter

def iterator(c,threshold,max_iter):  # in the condition which the absolute value of Z is less than the threshold value, we calculate the value of z within the maximum number of iterations
    z=c 
    for iter in range(0,max_iter,1): 
        if abs(z)>threshold:break
        z=z**2+c 
    return iter 

def plot_mandelbrot():    # give the input at the range [-2,1] x [-1.5,1.5], and plot the graph.
    x,y = np.mgrid[-2:1:50j, -1.5:1.5:50j]
    c=x+1j*y
    z = np.frompyfunc(iterator, 3, 1)(c, 50, 100).astype(np.float)
    plt.imshow(z.T, extent=[-2, 1, -1.5, 1.5])
    plt.gray()
    plt.show()
    plt.savefig('mandelbrot.png')


if __name__=="__main__":
    plot_mandelbrot()
