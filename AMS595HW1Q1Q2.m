clear
clc

inside1 = 0; % set the time that the random point is in side the 1/4 circle and initiate it with 0
count1 = 10000; % set we create 10000 random point totally
figure(1);% make first figure

% Question 1

Pi = zeros(1,count1); % set the value the pi we get in 1000 times
time = zeros(1,count1);% set time (initiate with 0 same above)
tic; % count the time

for n1 =1:count1 % in the interval of 1 to 10000
    x = rand; % make variates x and y and let them be random numbers between 0,1
    y = rand;
    
    if sqrt(x^2+y^2) <= 1        
        inside1 = inside1 + 1;% if the point is in the area of the 1/4 circle, then the inside value increase by 1 
    end
    
    Pi(n1) = 4 *(inside1/n1);% use the total inside value divide by total times we create points, which is 10000, we get the value of Pi
    time(n1) = toc;% count time
end

subplot(1,2,1);% creat first subplot with the value of Pi we get while time increase and the difference between the real value of pi and the pi we get 
plot([1:count1],Pi);
hold on
plot([1:count1],pi-Pi);
xlabel('Number of Random Points');
hold off
legend('Value of simulated Pi','Difference from the true value');
subplot(1,2,2);% create the secound subplot that with time and difference of real pu and pi we got
plot(time,pi-Pi);
xlabel('Computational Cost');
ylabel('Difference from the true value');

% Question 2  
inside2 = 0;
count2 = 10000;
Pi2 = zeros(1,count2);% set a value of pi in matrix and initiate them with 0
n2 = 1;

while n2 <= count2% while the n2 is smaller or equal to 10000, there will be point in position(x1,y1)
     x1 = rand;
     y1 = rand;
    
     if sqrt(x1^2+y1^2) <= 1
         inside2 = inside2 + 1;
     end
     
     Pi2(n2) = 4 * (inside2/n2);% save all different value of pi2 in different times
     
     if n2 > 1
         if round(Pi2(n2)*1000)/1000 == round(Pi2(n2-1)*1000)/1000% we calculate what time is the enough time that thrid digit can be assure 
             fprintf('%d steps are required for precision of 3 figures\n',n2);
             return
         end
     end
     n2 = n2 + 1;
end