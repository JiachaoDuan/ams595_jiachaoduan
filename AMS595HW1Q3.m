function [Pi] = pi_prcs(p)

inside = 0;% set the inside count and 2 values of pi
count = 0;
Pi = -1;
Pi_temp = -2;

while (Pi ~= Pi_temp || (p~=1 && Pi == 4))% for pi does not equal to pi_temp ,and p cannot choose 1 because it have too less points to estimate the Pi and the max estimate value of Pi is 4 ( (4* 1/1))
     Pi_temp = Pi;
     count = count + 1;
     x = rand;
     y = rand;
     if sqrt(x^2+y^2) <= 1
         inside = inside + 1;
         plot(x,y,'b.');
     else
         plot(x,y,'r.');
     end
     hold on
     Pi = round(4*(inside/count)*10^p)/10^p; % count the p-digits for estimate value of pi  
end

text(0.4,0.4,num2str(Pi),'FontSize',25);% print the graph