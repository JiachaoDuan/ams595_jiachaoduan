#AMS595 project 3
#Jiachao Duan 110177654
import numpy as np

def normalization(data):
    _range = np.max(data)-np.min(data)
    data = (data-np.min(data))/_range
    
    return data/np.sum(data)
# initialized and normalized the P and p
P = np.random.rand(5,5)
p = np.random.rand(5)
# according to the equation and calculate the p50
for i in range(5):
    P[i,:] = normalization(P[i,:])
p = normalization(p)

for i in range(50):
    p_ = np.dot(P.T,p)

# calculate the stationary distribution
value,mat = np.linalg.eig(P.T)
for i in range(5):
    if (1-value[i])<1e-6:
        vector = mat[i,:]
        break
''' print(np.abs(np.sum(p_)))
print(np.abs(np.sum(vector))) '''
err = np.abs(np.abs(np.sum(p_))-np.abs(np.sum(vector))) # set the err to the difference between the p50 and stationary distribution
''' print(err) '''
