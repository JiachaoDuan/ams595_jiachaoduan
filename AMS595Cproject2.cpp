#include<iostream>
#include<cmath>
#define PI 2*acos(0.0)
using namespace std;
// setting the number from the background because using the methoed we get Pi/4 so we need times 4 after get using the value
const int multi_num=4;
// the result should devides two
const double divide_num=2.0;

// first set the function to calculate the Pi/4 by using the first equation
double calculate_piover4(double x) {
    return sqrt(1-x*x);
}

// get the approximate pi by given steps
double compute_pi_approximate(int steps) {//initialize the size of step, sum value, xk, xk-1
    double step_size = 1.0 / steps;
    double sum_value = 0.0, xk = 0.0, xk1=0.0;
    int n =1;
    while(n<= steps) {
        xk1 = n*step_size;
        double functionV = (calculate_piover4(xk1) + calculate_piover4(xk)) / divide_num;
        sum_value += step_size * functionV;
        xk = xk1;
        ++n;
    }
    // return the approximate pi
    return multi_num*sum_value;
}

double approximatevaluepi(double toleranceerror,int start_steps,int steps_interval) {
    // calculate the approximated value based on the tolerance
    double approxvaluepi= compute_pi_approximate(start_steps);
    start_steps += steps_interval;
    while (abs(approxvaluepi - PI) > toleranceerror){
        approxvaluepi= compute_pi_approximate(start_steps);
        start_steps += steps_interval;
    }
    cout << "used steps: " << start_steps << endl;
    return approxvaluepi;
}

int main() {
    //define a property to accept user input
    double acceptableerrorvalue;
    //define steps start with 1000 and add 500 every step
    int base=1000,addingpart = 500;
    cout << "please input a maximum acceptable error to calculate pi: " << endl;
    cin >> acceptableerrorvalue;
    //get pi
    double pi = approximatevaluepi(acceptableerrorvalue,base,addingpart);
    //print pi
    cout << "the approximate PI result is: " << pi << endl;
    double error = abs(pi - PI);
    cout << "error: " << error;
    
}
